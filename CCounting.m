%% CC Algorithm
% use the dataset from cycle.mat
% Jian Hu
% 02/02/2022



clear;
sirdir = dir('C:\Users\mikeh\OneDrive\桌面\SOH_Darya\SOHC\cycle/2017');
max_DCV = 0;   %% Define the maximum OCV used to calculate the Ah that needs to be compensated for each Cycle，which the DCV smaller than max_DCV.[voltage filtering]
for i = 3:length(sirdir)
      load (['C:\Users\mikeh\OneDrive\桌面\SOH_Darya\SOHC\cycle/2017/' sirdir(i).name]);
       if data.DCV > max_DCV
          max_DCV = data.DCV;
        end
end





Cn = 48;     %% norminal Capacity is 50Ah.[voltage filtering]
cyc = 1;
cut = 3;
dt = 0.1;    %sampling rate

while 1
    if i > length(sirdir)
        break;
    end
flag = 0;
Ah_rp = [];   % initial throughput of relaxation point
V_rp = [];    % initial voltage of relaxation point. 
Ah_rps = [];  % initial throughput of relaxation point standing
V_rps = [];   % initial voltage of relaxation point standing
Ah_last = 0;  % initial throughput of the previous trip
temperature = [];  %%  to record the whole temperatures in one cycle [Temperature filtering]
rp_dvdt = [];      %% record dv/dt(end) in one cycle  [Quality filtering]
rp_dVdT10 = [];    %% record dV/dT(10)  in one cycle [Quality filtering]
rps_duration = [];  %% record duration of RPS in one cycle  [Quality filtering]
   for i = cut:(length(sirdir))
      load (['C:\Users\mikeh\OneDrive\桌面\SOH_Darya\SOHC\cycle/2017/' sirdir(i).name])

     if data.cycle == cyc   
         temperature = [temperature,data.temp];   %%add temperature  [Temperature filtering]
         DCV = data.DCV; %% determine the Driving Cycle start Voltage [voltage filtering]
         Ah_singel = 0;                               % initial throughput of singeltrip
         Ah_interval = [];                            % throughput change in singel trip

         rp_dvdt = [rp_dvdt,rp.dvdt_end];      %%  [Quality filtering]
         rp_dVdT10 = [rp_dVdT10, rp.dVdT_10];    %%  [Quality filtering]
         rps_duration = [rps_duration, rps.duration];  %% [Quality filtering]

         for k = 1:length(data.current)
             Ah_singel = Ah_singel + data.current(k)*dt/3600;
             Ah_interval = [Ah_interval,Ah_singel];
         end

         if ~isempty(data.point)                       % record the throughput and voltage of each point in one cycle
             for j = 1:length(data.point)
             if data.point(j) == 1
              Ah_rps = [Ah_rps, Ah_interval(data.point(j))+Ah_last];  %for relaxation point standing
              V_rps  = [V_rps, data.voltage(data.point(j))];
             else 
              Ah_rp = [Ah_rp, Ah_interval(data.point(j))+Ah_last];    %for relaxation point
              V_rp  = [V_rp, data.voltage(data.point(j))];
             end
             end
         end

         Ah_last = Ah_last + Ah_interval(end);         % update the previous throughput

          if i == length(sirdir)                             % Jump out of the for loop when this trip is the last.
          save(['C:\Users\mikeh\OneDrive\桌面\SOH_Darya\SOHC\rp_rps_analyse\2017/' num2str(cyc,"%03d") ...
              'cycle.mat'],"Ah_last","Ah_rps","V_rps","V_rp","V_rp","Ah_rp","DCV","rp_dvdt","rp_dVdT10","rps_duration");
          flag = 1;
          break
          end


         continue;

     elseif data.cycle ~= cyc    % when this trip is not the same cycle, then jump out of the for loop
          temperature(isnan(temperature)) = [];  %%[Temperature filtering]
          OCVresults = getOCV(mean(20));
          %OCVresults = getOCV(mean(temperature));   %% remove NaN and get the OCV curve under the mean temperature [Temperature filtering]
         [~,index1] = min(abs(OCVresults(:,2)- DCV));                                        %% find the index of SOC with DCV by OCV curve for this trip. [voltage filtering]
          [~,index2] = min(abs(OCVresults(:,2)- max_DCV));                                    %% find the index of SOC with max_DCV by OCV curve as a standard   [voltage filtering]
          delta_Ah = -(OCVresults(index2,1)/100-OCVresults(index1,1)/100) * Cn;                %% (deltaSOC * norminal Capacity) Calculate delta_Ah by comparing it with the Cycle of the maximum DCV [voltage filtering]
          if ~isempty(Ah_rps)                                                %% add the delta_Ah to each points [voltage filtering]
              Ah_rps = Ah_rps + delta_Ah;
          end
          if ~isempty(Ah_rp)
              Ah_rp = Ah_rp + delta_Ah;
          end

          save(['C:\Users\mikeh\OneDrive\桌面\SOH_Darya\SOHC\rp_rps_analyse\2017/' num2str(cyc,"%03d") ...
              'cycle.mat'],"Ah_last","Ah_rps","V_rps","V_rp","V_rp","Ah_rp","DCV","rp_dvdt","rp_dVdT10","rps_duration");    % save the data of each cycle
          cyc = cyc+1;                          % update the cycle number               %% for save add the value of DCV
          cut = i;                             % update the satrt value of i
          break
      end
   end
      if flag == 1
          break;
      end
end



%% Quality filtering

n = 8;             %  amount of bins, Every 6Ah of 48Ah.
dvdt_min = 0.01;    % minimum quality v/s for RP
dur_min  = 10;    % minimun duration for RPS (1 minute)
dvdt_opt = 0.001;   % optimal quality
dur_opt  = 10*60;   % optimal duration (10 min)


RP_filtering_Ah = [];           % Throughtput of relaxation point
RP_filtering_V = [];            % Voltage of relaxation point
RP_filtering_dvdt = [];         % dv/dt_filter of relaxation point
RP_filtering_n = [];            % Number of bin of relaxation point
RPS_filtering_Ah = [];          % Throughtput of relaxation point standing
RPS_filtering_V = [];           % Voltage of relaxation point standing
RPS_filtering_duration = [];    % Duration of relaxation point standing
RPS_filtering_n = [];           % Number of bin of relaxation point standing

simdir = dir('C:\Users\mikeh\OneDrive\桌面\SOH_Darya\SOHC\rp_rps_analyse\2017');
for  i = 3:length(simdir)
    load (['C:\Users\mikeh\OneDrive\桌面\SOH_Darya\SOHC\rp_rps_analyse\2017/' simdir(i).name]);
    % for rp with minimal accepted quality
    dvdt_filt = min(rp_dvdt,rp_dVdT10);    % choose the minimum value bewteen "dv/dt_end" and "dV/dT_10" as "dv/dt_filter"
    indr = find(dvdt_filt <= dvdt_min);
        RP_filtering_n= [RP_filtering_n, abs(fix((Ah_rp(indr)./(Cn/n))))+1];
        RP_filtering_V = [RP_filtering_V, V_rp(indr)];
        RP_filtering_Ah = [RP_filtering_Ah, Ah_rp(indr)];
        RP_filtering_dvdt = [RP_filtering_dvdt, dvdt_filt(indr)];

    % for rps with minimal accepted quality
    inds = find(rps_duration >= dur_min);
        RPS_filtering_Ah = [RPS_filtering_Ah, Ah_rps(inds)];
        RPS_filtering_V = [RPS_filtering_V,V_rps(inds)];
        RPS_filtering_duration = [RPS_filtering_duration, rps_duration(inds)];
        RPS_filtering_n = [RPS_filtering_n, abs(fix((Ah_rps(inds)/(Cn/n))))+1];
end


RP(:,1) =  RP_filtering_n;
RP(:,2) =  RP_filtering_Ah;
RP(:,3) =  RP_filtering_V;
RP(:,4) =  RP_filtering_dvdt;
RPS(:,1) =  RPS_filtering_n;
RPS(:,2) =  RPS_filtering_Ah;
RPS(:,3) =  RPS_filtering_V;
RPS(:,4) =  RPS_filtering_duration;

[RP_new,RPS_new] = filt(RP,RPS,n,dvdt_opt,dur_opt);  % get the points after filtering


%% Weighting
% weight for each bin is 1, see it in the function Fitness and ObjFun. 
n_max = [];    % find the max bin number of all Points
if ~isempty(RPS_new)
    if max(RP_new(:,1)) >= max(RPS_new(:,1))  
    n_max = max(RP_new(:,1));
    else
    n_max = max(RPS_new(:,1));
    end
else  n_max = max(RP_new(:,1));
end


%% OCV curve fitting
% here use the Cuckoo algorithm to search the best nest value with the lowerst RMSE.
newOCV = [-(Cn * OCVresults(:,1)/100)+Cn, OCVresults(:,2)];  %use the measured new OCV curve as a basis and convert SOC to Capacity.
% boundaries
a.l = -20;      % depends on the Capacity Age's level. [Ah]
a.r = 0;
b.l = 0.8;
b.r = 1.1;
c.l = 0;
c.r = 0;
d.l = 1;
d.r = 1;
lb          = [a.l, b.l, c.l, d.l];
ub          = [a.r, b.r, c.r, d.r];
popsize    = 25;        % Number of nests(solutions)
maxgen     = 200;       % Maximum number of iterations
N          = length(lb);% Number of identified parameters

% Discovery rate of alien eggs/solutions
pa1        = 1;
pa2        = 0.005;

% Calculate the fitness value of initial solutions
Value1     = ones(popsize,1);% Fitness value: error between measured OCV and data voltage, which is the Voltage of Points. 

% Run the CS
% Random initial solutions
nestmat    = genChrome(popsize,N,lb,ub); 

parfor i=1:popsize
   Value1(i,:) = fitness(nestmat(i,:),RPS_new,RP_new,newOCV,n_max);       
end

[vmin,indexmin] = min(Value1);% Evaluate initial solutions
bestnest        = nestmat(indexmin,:);% Get the current best solution
bestvalue       = vmin;% Get the current best fitness Value

% Initalize best solution and fitness value Matrixes of each generation
var_tracemat    = zeros(maxgen,N);
tracecsomat     = zeros(maxgen,1);

tic;% 
for i=1:maxgen
    
    % Generate new solutions using adaptive Levy Flight   
    new_nestmat = get_cuckoos(nestmat,popsize,bestnest,lb,ub,maxgen,i);
    % Evaluate new solutions and keep the current best
    parfor j=1:size(nestmat,1)
        [nestmat(j,:),Value1(j,1)] = ObjFun(nestmat(j,:)...
        ,new_nestmat(j,:),RPS_new,RP_new,newOCV,Value1(j,1),n_max);
    end
    
    % Discovery and randomization using adaptive discovery rate
    new_nest    = empty_nests(nestmat,N,lb,ub,pa1,pa2,i,maxgen);
    % Evaluate these solutions and keep the best
    parfor j=1:size(nestmat,1)
        [nestmat(j,:),Value1(j,1)] = ObjFun(nestmat(j,:)...
        ,new_nestmat(j,:),RPS_new,RP_new,newOCV,Value1(j,1),n_max);
    end
    
    % Get the current best value
    [vmin,indexmin] = min(Value1);
    % Find the best objective so far
    if bestvalue>vmin
        bestvalue = vmin;
        bestnest  = nestmat(indexmin,:);
    end
    
    % Record best solution and value
    tracecsomat(i,1)  = bestvalue;
    var_tracemat(i,:) = bestnest;
    
   showresults_step1(bestnest,i,bestvalue);
   
end


%% Figure OCV curve
OCV_x = (newOCV(:,1)+bestnest(1)).*bestnest(2);
OCV_y = (newOCV(:,2)+bestnest(3)).*bestnest(4);
OCV_ageing = [OCV_x,OCV_y];
plot(OCV_ageing(1001:end,1),OCV_ageing(1001:end,2))
hold on
plot(newOCV(1001:end,1),newOCV(1001:end,2));
xlim([0 50])
ylim([newOCV(1001,2) 4.2])
title('fitting result')
xlabel 'discharge Capacity'
ylabel 'Voltage'
%% 
%x = [-RP_new(:,2) ;-RPS_new(:,2)];
%y = [RP_new(:,3);RPS_new(:,3)];


 



%% Figure

% scatter to see the distribution
scatter(-RPS_new(:,2),RPS_new(:,3),70,"red","filled",'s')
hold on;
scatter(-RP_new(:,2),RP_new(:,3),30,'blue','filled','v')
grid on
xlim ([-10 50])
hold off
xlabel 'Discharge Ah'
ylabel 'Voltage'
title ([num2str(length(RP_new)+length(RPS_new)) ' points-2018'])
legend('relaxation point standing','relaxation point')







