function new_nestmat=get_cuckoos(nestmat,popsize,bestnest,lb,ub,maxgen,i)
% Levy flights
% Levy exponent and coefficient
beta=3/2;
sigma=(gamma(1+beta)*sin(pi*beta/2)/(gamma((1+beta)/2)*beta*2^((beta-1)/2)))^(1/beta);
for j=1:popsize
    s=nestmat(j,:);
    %% Levy flights by Mantegna's algorithm
    u=randn(size(s))*sigma;
    v=randn(size(s));
    step=u./abs(v).^(1/beta);
    stepsize=step.*(s-bestnest);
%     steplong=amax*maxgen/(a1+1)-amax*maxgen.^2/(a1+1).^2;
%     if steplong<=amin
%         steplong=amin;
%     end
%     s=s+stepsize.*steplong;

    % Adaptive factor related to stepsize
    alpha=0.25*exp(-0.5*0.5*(2*pi)^0.5*(i/maxgen)^2);
    % Get the new Nests
    s=s+stepsize.*alpha;
    x=limitfun(s,lb,ub);
    new_nestmat(j,:)=x;
end
