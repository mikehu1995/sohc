% This Function made for calculating the error(RMSE)
% Jian Hu
% 04/02/2022

function [Value1] = fitness(nestmat,RPS_new,RP_new,newOCV,n_max)

faktor = 0.5;     % weigt of relaxation point in one bin.(for RPS is relativ 1)

a = nestmat(1,1);
b = nestmat(1,2);
c = nestmat(1,3);
d = nestmat(1,4);
Value1 = [];
OCV_x = (newOCV(:,1)+a).*b;        % Use the formula 4.1 and 4.2 in the thesis 
OCV_y = (newOCV(:,2)+c).*d;
OCV_rebuild = [OCV_x,OCV_y];

for i = 1 : n_max      % calculation the SSE of each bin.
    OCV_V_RP = [];     % Record the voltage corresponding to each point on the measured OCV curve.
    OCV_V_RPS = [];
    SSEw_RPS = 0;      % initial SSE
    SSEw_RP = 0;

    indexRP = find(RP_new(:,1) == i);        % find the index of points which in n bin.
    RP_Throughput = -RP_new(indexRP,2);      % record the througput of points
    RP_V     = RP_new(indexRP,3);            % record the voltage of points

    for j = 1: length(RP_Throughput)
    [~,indexo1] = min(abs(OCV_rebuild(:,1)-RP_Throughput(j)));    % find the index corresponding to each point on the measured OCV curve. 
    OCV_V_RP = [OCV_V_RP; OCV_rebuild(indexo1,2)];
    end

    indexRPS = find(RPS_new(:,1) == i);
    if ~isempty(indexRPS)
        RPS_Throughput = -RPS_new(indexRPS,2);
        RPS_V     = RPS_new(indexRPS,3);
        for k = 1: length(RPS_Throughput)
         [~,indexo2] = min(abs(OCV_rebuild(:,1)-RPS_Throughput(k)));
         OCV_V_RPS = [OCV_V_RPS; OCV_rebuild(indexo2,2)];
        end
        SSEw_RPS = sum(((RPS_V - OCV_V_RPS).^2));
    end


SSEw_RP = sum((0.5*(RP_V - OCV_V_RP).^2));    % Calculate the SSE of RP with faktor.
Error = ((SSEw_RP +SSEw_RPS)/ (0.5*length(OCV_V_RP)+length(OCV_V_RPS))); % average SSE of each bin.
Value1 = [Value1, Error];
end
Value1 = (sum(Value1)/length(Value1))^0.5;     % RMSE




end
