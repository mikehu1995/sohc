function [vmin,bestnest,nestmat,Value]=get_best_nest(nestmat,new_nestmat,Value,Icurrents,timings,param,test)
% Evaluating all new solutions
parfor j=1:size(nestmat,1)
    [nestmat(j,:),Value(j,1)] = ObjFun(nestmat(j,:),new_nestmat(j,:),Icurrents,timings,param,test,Value(j,1));
end
% Find the current best value and solution
[vmin,indexmin]=min(Value);
bestnest=nestmat(indexmin,:);
end