% This function made for Figure 4.8 quality filtering
% Jian Hu   
% 04/02/2022


function [RP_new,RPS_new] = filt(RP,RPS,n,dvdt_opt,dur_opt)

num_target = 30;               %number of target
RP_new = [0,0,0,0]; 
RPS_new = [0,0,0,0];

for i = 1:n
    nrp = [];
    nrps = [];
    flag = 0;
    index = find(RP(:,1) == i);              % find the point in i bin
    indes = find(RPS(:,1) == i);             % find the point standing in i bin
    for j = 1:10                                % softens the boundaries:
        dvdt = dvdt_opt + (0.0001*(j-1));           % requirement of dv/dt from 0.0001 to 0.001 (from optimal to minimum)
        duration  = dur_opt * (1.1-(j*0.1));        % requirement of duration of relaxation point standing from 10min to 1min.
        nrp = find(RP(index,4) <= dvdt);        % find the point when meet the requirements
        nrps=find(RPS(indes,4) >= duration);
        if length(nrp) + length(nrps) >= num_target   % when the Num_target are enough, jump out the loop and check the next bin.
            RP_new = [RP_new; RP(index(nrp),:)];      % record the point data.
            RPS_new = [RPS_new, RPS(indes(nrps),:)];
            flag = 1;
            break
        end
    end
    if flag == 0                                      %  At low Soc, Num_traget not reachable. All avaiable points which meet the minimum quality taken into account.
            RP_new = [RP_new; RP(index(nrp),:)];
            RPS_new = [RPS_new;RPS(indes(nrps),:)];
    end
   
end
RP_new = RP_new(2:end,:);                 % remove the initial [0,0,0,0]
RPS_new = RPS_new(2:end,:);
end





