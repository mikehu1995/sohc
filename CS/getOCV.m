%Skript to get OCV-SOC-Temperature from iMiev-Data
%All Date are from Labor
%2022/01/02
%Jian Hu
%clc;
%clear;

function OCVresults = getOCV(Temperature) 
load miev_electrical.mat;

ocvinput = eleDatabaseModel{1,1}.CustomDefinitions.MyOCV.Object.LookupData.Text;
ocv = str2num(ocvinput);

SOC = [-10:5:100];

ThermalState = [-15,-5,5,15,25,35,45];


%Interpolation
soc1 = [-10:0.01:100];
temp1 = [-15:1:45];
[soc_inter,temp_inter] = meshgrid(soc1,temp1);
ocv_inter = interp2(SOC,ThermalState,ocv,soc_inter,temp_inter,'cubic');

%get OCV Curve at the certain temperature
Temperature(isnan(Temperature)==1) = 0;
temp = round(mean(Temperature));

a = ocv_inter(temp,:); %get the OCV curve unter the operation Temperature
OCVresults(:,1) = soc1';
OCVresults(:,2) = a';

end


%Table


%%figure(1);
%surf(soc_inter,temp_inter,ocv_inter);
%title({'OCV-SOC-Temperature'});
%xlabel('SOC');
%ylabel('Temperature');
%zlabel('OCV');

%%figure(2);
%surf(SOC,ThermalState,ocv);
%title({'R0-SOC-Temperature'});
%xlabel('SOC');
%ylabel('Temperature');
%zlabel('R0');



