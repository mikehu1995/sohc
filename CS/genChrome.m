function nestmat=genChrome(popsize,N,lb,ub)
% Random initial solutions
nestmat=zeros(popsize,N);
for i=1:popsize
    nestmat(i,:)=lb+(ub-lb).*rand(size(lb));
end
