function new_nest=empty_nests(nestmat,N,lb,ub,pa1,pa2,i,maxgen)

% a=sum(Value);
% fmin=min(Value);
% favg=a/popsize;
% A=zeros(popsize,N);
% B=zeros(popsize,N);
% C=zeros(popsize,N);
% D=zeros(popsize,N);
% E=zeros(popsize,N);
% R1=zeros(popsize,N);
% pa=zeros(popsize,N);
% for j=1:popsize
%     if Value(j)<favg
%         A(j,:)=nestmat(j,:);
%         B(j,:)=zeros(1,N);
%         C(j,:)=Value(j);
%     else
%         A(j,:)=zeros(1,N);
%         B(j,:)=nestmat(j,:);
%         C(j,:)=0;
%     end
%     if C(j)>0
%         pa(j,:)=pa2+(pa1-pa2)*(C(j,:)-fmin)/(favg-fmin);
%     else
%         pa(j,:)=0;
%     end
%     
% end
% SJ=rand(size(A));
% for j=1:popsize
%     D(j,:)=A(j,:)~=0;
%     E(j,:)=B(j,:)~=0;
% end
% 
% for j=1:popsize
%     R1(j,:)=SJ(j,:)<pa(j,:);
% end
% r1=R1.*D;
% stepsize=rand*(A(randperm(popsize),:)-A(randperm(popsize),:));
% new_nest1=A+stepsize.*D.*r1;
% 
% R2=rand(size(B))<pa1;
% r2=R2.*E;
% stepsize=rand*(B(randperm(popsize),:)-B(randperm(popsize),:));
% new_nest2=B+stepsize.*E.*r2;
% new_nest=new_nest1+new_nest2;

% A fraction of worse nests are discovered with a probability pa

%theta0p.l   = 0.5; %Stochiometric parameter of the cathode under 0% SOC
%theta0p.r   = 1.0;
% theta100p.l =0;     
% theta100p.r =0.2;
% theta0n.l   =0;     
% theta0n.r   =0.5;
%theta100n.l = 0.5; %Stochiometric parameter of the anode under 100% SOC
%theta100n.r = 1.0;

n=size(nestmat,1); 

% Discovered or not -- a status vector
% Adaptive discovery rate
pa=pa2+(pa1-pa2)*(i/maxgen)^N;
K=rand(size(nestmat))>pa;

% In real world, if a cuckoo's egg is very similar to a host's eggs, then 
% this cuckoo's egg is less likely to be discovered, thus the fitness should 
% be related to the difference in solutions.  Therefore, it is a good idea 
% to do a random walk in a biased way with some random step sizes.

% Randomized Nests
nestn1=nestmat(randperm(n),:);
nestn2=nestmat(randperm(n),:);
%% New solution by biased/selective random walks
stepsize=rand*(nestn1-nestn2); 
new_nest=nestmat+stepsize.*K; 
for j=1:size(new_nest,1)
    s=new_nest(j,:);
    s=limitfun(s,lb,ub);% Limits of nests(solution)
    new_nest(j,:)=s;
end



