# SOHC

Here shows all the data and files used in the method.

**data2017**
_The raw data of iMiev trips in 2017_

**code: cycle**
_This script is for cyclic division and get informations about relaxation point according to the data._

**file: cycle**
_The file to save the results after cycling_

**code: CCounting**
_main code_

**file: re_data2017 & re_rps_analyse**
_Files used in main code.
File re_rps_analyse saves the relaxation point results_


**file CS**
_functions used in main code_

