% This script is for cyclic division and get informations about relaxation 
% point according to the data.
% All data files should be numbered in consecutive numerical order. 
% The data should include :   wohle Current, 
%                             Voltage, 
%                             whole temperature,
%                             stV(start Voltage),
%                             endV(end Voltage),
%                             stC(start current),
%                             endC(end current),
%                             start time.
% Jian Hu
% 01/02/2022


clear;
clc;

subdir = dir('C:\Users\wli-jhu\Desktop\SOH_Darya\SOHC\re_data2020\');        % path of databank
Vfull = 4.03;            % voltage of full charge
deltaI = 3;              % delta current
Vdiffend = 0.05;          % diffend Voltage
cyc = 1;                 % intial cycle number
cut = 3;                 % initail start. 
flag =0;
i = 3;
while i <= length(subdir)
        disp(["Trip ", num2str(i-2) " of " num2str(length(subdir)-2)])
        load ([subdir(1).folder '\' subdir(i).name])
        if ~isempty(find(isnan(data.current))) || isempty(data.voltage)                % Ignore this set of data if there are NaNs in the data
            i = i+1;
            if flag == 1
                flag = 0;
                cyc = cyc +1;
            end
            continue;
        end
        if (data.stV < Vfull || (-deltaI) >= data.stC || data.stC >= deltaI) && flag == 0               %if not meet the requirement then ignore 
                % Ignore this set of data if there are NaNs in the data                  % this data until meet the requirement.
                i = i+1;
            continue;
        elseif flag == 0
            data.cycle = cyc;
            data.DCC = data.current(1);           % Driving Cycle start Current.
            data.DCV = data.voltage(1);           % Driving Cycle start Voltage.
            DCC = data.current(1);
            DCV = data.voltage(1);
            [rp,rps,data] = relax(data,deltaI);
            last_voltage = data.voltage(end);
            flag = 1;
            save(['C:\Users\wli-jhu\Desktop\SOH_Darya\SOHC\cycle\2020\' num2str(cyc,"%03d"),'_Trip_' num2str(i) '.mat'],"data","rps","rp");
            i = i+1;
        elseif flag == 1
            data.cycle = cyc;
            data.DCC = DCC;           % Driving Cycle start Current.
            data.DCV = DCV;           % Driving Cycle start Voltage.
            [rp,rps,data] = relax(data,deltaI);
            if ~isempty(rp.voltage)
                if ~((data.voltage(1) - last_voltage) > Vdiffend || max([rp.voltage, rps.voltage]) - last_voltage > Vdiffend)
                    last_voltage = data.voltage(end);
                    save(['C:\Users\wli-jhu\Desktop\SOH_Darya\SOHC\cycle\2020/' num2str(cyc,"%03d"),'_Trip_' num2str(i) '.mat'],"data","rps","rp");
                    i = i+1;
                else
                    flag =0;
                    cyc = cyc +1;
                    continue;
                end
            else
                cyc = cyc+1;
                flag =0;
                i = i+1;
            end    
        end
        
       
    
    
end




